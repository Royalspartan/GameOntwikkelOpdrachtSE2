﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace OntwikkelOpdrachtSE2Game
{
    class Map
    {
        //velden
        Characters.Player player;
        List<Cell> Cells = new List<Cell>();
        List<Characters.Bullet> Bullets = new List<Characters.Bullet>();
        private int bulletcounter = 0;
        private bool enemyEnabled;
        private bool playerIsDead;
        

        //props
        public Size MapSize { get; set; }
        public Size CellSize { get; set; }
        public Point CellCount { get; set; }

        //operators
        public Map(Size size, Point cellCount)
        {

            MapSize = size;
            CellCount = cellCount;
            CellSize = new Size(size.Width / cellCount.X,
                                     size.Height / cellCount.Y);

            //Speler wordt aangemaakt
            player = new Characters.Player(CellSize);
            //Verdeelt de map in Cells en telt ze
            NumberCells();
        }

        //Map wordt getekend
        public void DrawMap(Graphics g)
        {
            foreach (Cell cell in Cells)
            {
                cell.DrawText(g, Cells.IndexOf(cell));
                cell.DrawCell(g);
            }
        }

        //Walls Worden in de map geplaast
        public void AddWalls(int[] CellsIndex)
        {
            foreach (int i in CellsIndex)
            {
                Cells[i].celltype = Cell.CellType.Wall;
            }
        }

        public void AddRoad(int[] CellsIndex)
        {
            foreach (int i in CellsIndex)
            {
                Cells[i].celltype = Cell.CellType.RoadUp;
                Cells[i + 1].celltype = Cell.CellType.RoadDown;
            }
        }

        public void AddFinishPoint(int CellIndex)
        {
            Cells[CellIndex].celltype = Cell.CellType.FinishPoint;
        }
        //Cars worden in de map geplaast
        public void AddCar(int CellIndex, int CellIndex2)
        {
            Cells[CellIndex].celltype = Cell.CellType.CarFront;
        }

        public void AddCharizard(int Cellindex)
        {
            Bullets.Add(new Characters.Bullet(CellSize, Cells[Cellindex].Position));
            Bullets[bulletcounter].EndLocation = player.Location;
            bulletcounter++;
            Cells[Cellindex].celltype = Cell.CellType.Charizard;
            enemyEnabled = true;
        }

        //Cars worden van de map verwijderd
        public void DeleteCar()
        {
            foreach  (Cell cell in Cells)
            {
                if (cell.celltype == Cell.CellType.CarFront)
                {
                    cell.celltype = Cell.CellType.Empty;
                }
            }
        }

        //Map wordt verdeeld in Cells
        public void NumberCells()
        {
            int CellX = 0;
            int CellY = 0;
            for (int x = 0; x < CellCount.X; x++)
            {
                CellY = 0;
                for (int y = 0; y < CellCount.Y; y++)
                {
                    Cells.Add(new Cell(new Point(CellX,CellY),CellSize));
                    CellY = CellY + CellSize.Height;
                }
                CellX = CellX + CellSize.Width;
            }
        }

        //Verplaast Speler en kijk of hij in een auto kan
        public void MovePlayer(int Direction)
        {
            player.Move(Direction);
            foreach (Cell cell in Cells)
            {
                if (player.Location == cell.Position && cell.PlayerCollision == true)
                {
                    if (cell.celltype == Cell.CellType.CarFront && player.InCar == false)
                    {
                        player.InCar = true;
                        player.ImgPlayer = OntwikkelOpdrachtSE2Game.Properties.Resources.CarRechts;
                        DeleteCar();
                    }
                    else if (cell.celltype == Cell.CellType.RoadUp && player.InCar == true)
                    {
                    }
                    else
                    {
                        if (Direction == 1) { player.Move(2); }
                        if (Direction == 2) { player.Move(1); }
                        if (Direction == 3) { player.Move(4); }
                        if (Direction == 4) { player.Move(3); }
                    }
                }
            }                
        }

        public void MoveBullet()
        {
            foreach (Characters.Bullet Bullet in Bullets)
            {
                Bullet.MoveToPlayer();
                foreach (Cell cell in Cells)
                {
                    if (Bullet.StartLocation == cell.Position && cell.celltype == Cell.CellType.Wall)
                    {
                        Bullet.EndLocation = Bullet.StartLocation;
                    }
                    if (Bullet.StartLocation == player.Location)
                    {
                        Bullet.EndLocation = Bullet.StartLocation;
                        player.Location = new Point(CellSize.Width, CellSize.Width);
                        playerIsDead = true;
                    }
                }
            }
            if (playerIsDead == true) { Bullets.Clear(); bulletcounter = 0; playerIsDead = false; }
        }

        //tekent de speler
        public void DrawCharacters(Graphics g)
        {
            g.DrawImage(player.ImgPlayer, player.Location.X,player.Location.Y,CellSize.Width,CellSize.Height);
            if (enemyEnabled == true)
            {
                foreach (Characters.Bullet Bullet in Bullets)
                {
                    g.DrawImage(Bullet.imgBullet, Bullet.StartLocation.X, Bullet.StartLocation.Y, CellSize.Width, CellSize.Height); 
                }              
            }
        }

    }
}
