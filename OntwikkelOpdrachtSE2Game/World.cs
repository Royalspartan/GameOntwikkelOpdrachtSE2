﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace OntwikkelOpdrachtSE2Game
{
    class World
    {

        //props
        public Map Map { get; private set; }


        public World(Size mapSize, Point cellCount)
        {
            Map = new Map(mapSize, cellCount);
        }

        public void KeyDirection(int Direction)
        {
            Map.MovePlayer(Direction);
        }
        
        public void Update()
        {
        }

        public void Draw(Graphics g)
        {
            //Let op z-index
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            //Niet vergeten om drawplayer als laast te zetten. Om de coordinaten te bekijken is dit nog even handig
            Map.DrawMap(g);          
            Map.DrawCharacters(g);
        }
    }
}
