﻿namespace OntwikkelOpdrachtSE2Game
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.UpdateTimer = new System.Windows.Forms.Timer(this.components);
            this.PlayerIMG = new System.Windows.Forms.ImageList(this.components);
            this.BulletTimer = new System.Windows.Forms.Timer(this.components);
            this.pboxGame = new System.Windows.Forms.PictureBox();
            this.BulletSpeed = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pboxGame)).BeginInit();
            this.SuspendLayout();
            // 
            // UpdateTimer
            // 
            this.UpdateTimer.Enabled = true;
            this.UpdateTimer.Interval = 20;
            this.UpdateTimer.Tick += new System.EventHandler(this.timerUpdate_Tick);
            // 
            // PlayerIMG
            // 
            this.PlayerIMG.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.PlayerIMG.ImageSize = new System.Drawing.Size(16, 16);
            this.PlayerIMG.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // BulletTimer
            // 
            this.BulletTimer.Interval = 1000;
            this.BulletTimer.Tick += new System.EventHandler(this.BulletTimer_Tick);
            // 
            // pboxGame
            // 
            this.pboxGame.BackColor = System.Drawing.Color.DarkGray;
            this.pboxGame.Location = new System.Drawing.Point(12, 12);
            this.pboxGame.Name = "pboxGame";
            this.pboxGame.Size = new System.Drawing.Size(600, 600);
            this.pboxGame.TabIndex = 0;
            this.pboxGame.TabStop = false;
            this.pboxGame.Paint += new System.Windows.Forms.PaintEventHandler(this.pboxGame_Paint);
            // 
            // BulletSpeed
            // 
            this.BulletSpeed.Interval = 150;
            this.BulletSpeed.Tick += new System.EventHandler(this.BulletSpeed_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 617);
            this.Controls.Add(this.pboxGame);
            this.Name = "Form1";
            this.Text = "GrandTheftPokemon";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.pboxGame)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pboxGame;
        public System.Windows.Forms.ImageList PlayerIMG;
        public System.Windows.Forms.Timer UpdateTimer;
        private System.Windows.Forms.Timer BulletTimer;
        private System.Windows.Forms.Timer BulletSpeed;
    }
}

