﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OntwikkelOpdrachtSE2Game
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            this.world = new World(pboxGame.Size, new Point(15,15));
        }
        //Velden1
        private World world;

        private void pboxGame_Paint(object sender, PaintEventArgs e)
        {
            this.world.Draw(e.Graphics);

            //Maakt een Array met daarin de coordinaten waar een muur komen. (met coordinaat wordt de celindex bedoelt) 
            int[] OuterWalls = new int[] { 0,15,30,45,60,75,90,105,120,135,150,165,180,195,210,1,2,3,4,5,6,7,8,9,10,11,12,13,14,29,44,59,74,89,104,119,134,149,164,179,194,209,224,211,212,213,214,215,216,217,218,219,220,221,222,223};
            world.Map.AddWalls(OuterWalls);
        }

        private void timerUpdate_Tick(object sender, EventArgs e)
        {
            //Refreshers
            this.world.Update();
            pboxGame.Refresh();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            //Laat speler bewegen (1) Beneden (2) Boven (3) Links (4) Rechts
            if (e.KeyCode == Keys.Down)
            {
                world.KeyDirection(1);
            }
            if (e.KeyCode == Keys.Up)
            {
                world.KeyDirection(2);
            }
            if (e.KeyCode == Keys.Left)
            {
                world.KeyDirection(3);
            }
            if (e.KeyCode == Keys.Right)
            {
                world.KeyDirection(4);
            }
            //
            //LevelLoader
            if (e.KeyCode == Keys.NumPad0)
            {
                int[] CellCordWalls = new int[] { 18, 33, 48, 63, 78, 93, 94, 95, 96, 97, 98, 99, 24, 39, 54,166,167,168,169,170,171,172,173,174,175,176,61};
                world.Map.AddWalls(CellCordWalls);

                int[] CellCordRoadUp = new int[] { 27,42,57,72,87,102,117,132,147,162,177,192,207};
                world.Map.AddRoad(CellCordRoadUp);
                world.Map.AddCharizard(156);
                world.Map.AddCharizard(160);
                world.Map.AddFinishPoint(181); world.Map.AddFinishPoint(196); world.Map.AddFinishPoint(182); world.Map.AddFinishPoint(197);
                world.Map.AddCar(19, 34);
                BulletTimer.Start();
                BulletSpeed.Start();
            }
        }

        private void BulletTimer_Tick(object sender, EventArgs e)
        {
            world.Map.AddCharizard(156);
            world.Map.AddCharizard(160);
        }

        private void BulletSpeed_Tick(object sender, EventArgs e)
        {
            world.Map.MoveBullet();
        }
    }
}
