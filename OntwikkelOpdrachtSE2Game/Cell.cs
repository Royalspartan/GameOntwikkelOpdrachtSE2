﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace OntwikkelOpdrachtSE2Game
{
    class Cell
    {
        public enum CellType
        {
            Wall,
            CarFront,
            RoadUp,
            RoadDown,
            Charizard,
            FinishPoint,
            Empty
        }

        //velden
        private Point position;
        private Size cellsize;

        //Props
        public Point Position { get { return position; } set { position = value; } }
        public bool PlayerCollision { get; set; }
        public bool CarCollision { get; set; }
        public CellType celltype { get; set; }

        public Cell(Point Position, Size Cellsize )
        {
            position = Position;
            cellsize = Cellsize;
            celltype = CellType.Empty;
            CarCollision = false;
        }

        public void DrawText(Graphics g, int index)
        {
            //Dit is methode die niet in het eindproduct komt, deze methode laat alle bijbehoorende coordinaten van een cell zien
            //En tekent ze. (Handig om maps te maken)
            String drawString = Convert.ToString(index);
            Font drawFont = new Font("Arial", 16);
            SolidBrush drawBrush = new SolidBrush(Color.Black);
            Point drawPoint = position;
            g.DrawString(drawString, drawFont, drawBrush, drawPoint);
        }
      
        public void DrawCell(Graphics g)
        {
            //Tekent alle cellen in de map. Hij is uit geschreven in If's om het voor mij overzichtelijker te maken
            //Later zal dit smarter opgeschreven worden
            if (celltype == CellType.Empty)
            {
                PlayerCollision = false;
                CarCollision = false;
            }
            if (celltype == CellType.Wall)
            {
                g.DrawImage(OntwikkelOpdrachtSE2Game.Properties.Resources.Stone, position.X, position.Y,cellsize.Width,cellsize.Height);
                PlayerCollision = true;
                CarCollision = true;
            }
            if (celltype == CellType.CarFront)
            {
                g.DrawImage(OntwikkelOpdrachtSE2Game.Properties.Resources.CarRechts, Position.X, Position.Y, cellsize.Width , cellsize.Height);
                PlayerCollision = true;
                CarCollision = true;
            }
            if (celltype == CellType.RoadUp)
            {
                g.DrawImage(OntwikkelOpdrachtSE2Game.Properties.Resources.road, Position.X, position.Y, cellsize.Width, cellsize.Height * 2);
                PlayerCollision = true;
                CarCollision = false;
            }
            if (celltype == CellType.Charizard)
            {
                g.DrawImage(OntwikkelOpdrachtSE2Game.Properties.Resources.Charizard, Position.X, Position.Y, cellsize.Width, cellsize.Height);
                PlayerCollision = true;
                CarCollision = true;
            }
            if (celltype == CellType.FinishPoint)
            {
                g.DrawImage(OntwikkelOpdrachtSE2Game.Properties.Resources.FinishPoint, Position.X, Position.Y, cellsize.Width, cellsize.Height);
            }
        }
    }
}
