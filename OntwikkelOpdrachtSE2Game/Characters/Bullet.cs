﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace OntwikkelOpdrachtSE2Game.Characters
{
    class Bullet
    {

        //velden
        private List<Image> imgListBullet = new List<Image>();//kk/kk
        Character chcBullet;

        //Props
        public Size CellSize { get; set; }
        public Point StartLocation { get; set; }
        public Point EndLocation { get; set; }
        public Point TijdelijkLocation { get; set; }
        public Image imgBullet { get; set; }

        public Bullet(Size SizeCell, Point startPosition)
        {
            imgListBullet.Add(OntwikkelOpdrachtSE2Game.Properties.Resources.Fire);
            imgBullet = OntwikkelOpdrachtSE2Game.Properties.Resources.Fire;
            StartLocation = startPosition;
            chcBullet = new Character(imgListBullet,SizeCell,startPosition);
        }

        public void MoveToPlayer()
        {

            if (EndLocation.X > StartLocation.X)
            {
                TijdelijkLocation = chcBullet.Move(4);
            }
            if (EndLocation.X < StartLocation.X)
            {
                TijdelijkLocation = chcBullet.Move(3);
            }
            if (EndLocation.Y > StartLocation.Y)
            {
                TijdelijkLocation = chcBullet.Move(1);
            }
            if (EndLocation.Y < StartLocation.Y)
            {
                TijdelijkLocation = chcBullet.Move(2);
            }
            if (EndLocation == StartLocation)
            {
                imgBullet = OntwikkelOpdrachtSE2Game.Properties.Resources.Blowup;
            }

            StartLocation = TijdelijkLocation;
        }
    }
}
