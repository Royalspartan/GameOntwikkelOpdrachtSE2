﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace OntwikkelOpdrachtSE2Game.Characters
{
    class Character
    {
        //velden
        List<Image> characterimage;
        private int index;
        Size cellSize;
        
        
        //props
        public int Index { get { return index; } set { index = value; } }
        public Point chcLocation { get; set; }

        public Character(List<Image> CharacterImage, Size CellSize, Point Location)
        {
            characterimage = CharacterImage;
            cellSize = CellSize;
            chcLocation = Location;
        }

        public Point Move(int Direction)
        {
            //Kijkt welke knop ingedrukt is en verplaast de Player of Enemy op basis van die gegevens
            if (Direction == 1)
            {
                index = 0;
                chcLocation = new Point(chcLocation.X, chcLocation.Y + cellSize.Height);
            }
            if (Direction == 2)
            {
                index = 1;
                chcLocation = new Point(chcLocation.X, chcLocation.Y - cellSize.Height);
            }
            if (Direction == 3)
            {
                index = 2;
                chcLocation = new Point(chcLocation.X - cellSize.Width, chcLocation.Y);
            }
            if (Direction == 4)
            {
                index = 3;
                chcLocation = new Point(chcLocation.X + cellSize.Width, chcLocation.Y);
            }
            return chcLocation;
        }

    }
}
