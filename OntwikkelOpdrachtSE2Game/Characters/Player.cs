﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;

namespace OntwikkelOpdrachtSE2Game.Characters
{
    class Player
    {
        //velden
        private new Point location;
        private List<Image> imgListPlayer = new List<Image>();
        private List<Image> imgListCar = new List<Image>();
        private Image imgPlayer;
        Characters.Character chcPlayer;

        //props
        public Point Location { get { return location; } set{location = value; } }
        public List<Image> ImgListPlayer { get { return imgListPlayer; } set { imgListPlayer = value; } }
        public Image ImgPlayer { get { return imgPlayer; } set { imgPlayer = value; } }
        public bool InCar { get; set; }

        public Player(Size Cellsize)
        {
            imgPlayer = OntwikkelOpdrachtSE2Game.Properties.Resources.Beneden; 
            imgListPlayer.Add(OntwikkelOpdrachtSE2Game.Properties.Resources.Beneden);
            imgListPlayer.Add(OntwikkelOpdrachtSE2Game.Properties.Resources.Boven);
            imgListPlayer.Add(OntwikkelOpdrachtSE2Game.Properties.Resources.Links);
            imgListPlayer.Add(OntwikkelOpdrachtSE2Game.Properties.Resources.Rechts);
            imgListCar.Add(OntwikkelOpdrachtSE2Game.Properties.Resources.CarBeneden);
            imgListCar.Add(OntwikkelOpdrachtSE2Game.Properties.Resources.CarBoven);
            imgListCar.Add(OntwikkelOpdrachtSE2Game.Properties.Resources.CarLinks);
            imgListCar.Add(OntwikkelOpdrachtSE2Game.Properties.Resources.CarRechts);
            location = new Point(Cellsize.Width, Cellsize.Height);
            chcPlayer = new Characters.Character(imgListPlayer, Cellsize, location);
        }
       
        
        public void Move(int Derection)
        {
            chcPlayer.chcLocation = location;
            //Verplaast de player en berekent nieuwe coordinaten met behulp van de Character klasse
            location = chcPlayer.Move(Derection);
            //Kijkt of de speler de auto heeft gepakt
            if (InCar == true)
            {
                imgPlayer = imgListCar[chcPlayer.Index];
                
            }
            else
            {
               imgPlayer = imgListPlayer[chcPlayer.Index];
            }
        }
      

    }
}
